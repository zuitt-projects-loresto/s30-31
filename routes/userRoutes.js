const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers');

router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUsersController);

router.put('/updateUser/:id', userControllers.updateUserController);

router.get('/getSingleUser/:id', userControllers.getSingleUserController);

module.exports = router;