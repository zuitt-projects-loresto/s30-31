const User = require('../models/User');

module.exports.createUserController = (req, res) => {

	console.log(req.body);

	User.findOne({name: req.body.name}).then(result => {
		if(result !== null && result.name === req.body.name){
			
			return res.send('Duplicate user found')

		} else {
			
			let newUser = new User({

				name: req.body.name,
				password: req.body.password

			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		}
	})
	.catch(error => res.send(error));

};

module.exports.getAllUsersController = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.updateUserController = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name : req.body.name
	};
	
	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.getSingleUserController = (req, res) => {

	console.log(req.params.id);

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}